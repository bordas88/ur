 Programa
   Programa de robot
     Gripper Activate
     Pinza abierta (1)
     MoverJ
       Punto_de_paso_2
     MoverJ
       Disparo
     Camera Locate
       Para los objeto(s) encontrados
         MoverJ
           Arriba1
         MoverL
           Abajo1
         Pinza cerrada (1)
         Controlar la pinza
           Si se detecta un objeto          
             MoverL
               Arriba1
             MoverJ
               Acerco
             Palé
               Patrón: Cuadrado
                 Esquina1_1
                 Esquina2_1
                 Esquina3_1
                 Esquina4_1
               SecuenciaPalé
                 Acercar_1
                 Punto_de_paso_1
                 Ajustar
                 Pinza abierta (1)
                 Esperar: 0
                 Salir_1
